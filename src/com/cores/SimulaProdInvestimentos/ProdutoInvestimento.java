/**
 *
 * Nome do programa:			ProdutoInvestimento.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	17/05/2020
 * Descrição:					Possui as informações do Produto de Investimento (Ex. RF, DI).
 */

package com.cores.SimulaProdInvestimentos;

public class ProdutoInvestimento {

	private double taxaInvestimento = 0.7 / 100;	//0,7%
	
	public double getTaxaInvestimento() {
		return taxaInvestimento;
	}
	
}
