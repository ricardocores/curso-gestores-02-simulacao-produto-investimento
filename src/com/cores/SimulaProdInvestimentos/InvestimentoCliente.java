/**
 *
 * Nome do programa:			InvestimentoCliente.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	17/05/2020
 * Descrição:					Possui informações do investimento do cliente
 */

package com.cores.SimulaProdInvestimentos;

public class InvestimentoCliente {
	
	double valorInvestimento		= 0;
	int quantidadeMesesInvestimento   	
									= 0;
	
	public void setValorInvestimento(double valorInvestimento) {
		this.valorInvestimento		= valorInvestimento;
	}
	
	public void setQtdeMesesInvestimento(int quantidadeMesesInvestimento) {
		this.quantidadeMesesInvestimento	
									= quantidadeMesesInvestimento;
	}
	
	public double getValorInvestimento() {
		return valorInvestimento;
	}
	
	public int getQuantidadeMesesInvestimento() {
		return quantidadeMesesInvestimento;
	}
	
}
