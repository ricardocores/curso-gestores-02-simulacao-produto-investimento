/**
 *
 * Nome do programa:			SimuladorValorInvestido.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	17/05/2020
 * Descrição:					Simula o valor investido pelo cliente após "n" meses (Juros compostos)
 */

package com.cores.SimulaProdInvestimentos;

public class SimuladorValorInvestido {
	
	double valorAcumulado			= 0;
	
	public double simularInvestimento(InvestimentoCliente investimento) {
		
		ProdutoInvestimento produto		= new ProdutoInvestimento();
		
		valorAcumulado					= investimento.getValorInvestimento();
		
		for (int i = 0; i < investimento.getQuantidadeMesesInvestimento(); i++) {
			valorAcumulado			    = (valorAcumulado * (produto.getTaxaInvestimento() + 1));
		}
		
		return valorAcumulado;
	}
}
