/**
 *
 * Nome do programa:			LeitorTerminal.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	17/05/2020
 * Descrição:					Lê informações do Terminal
 */

package com.cores.SimulaProdInvestimentos;

import java.util.Scanner;

public class LeitorTerminal implements LeitorInvestimento{
	
	Scanner scanner					= new Scanner(System.in);
	
	public double lerValorInvestimento() {
		return scanner.nextDouble();
	}
	
	public int lerQuantidadeMesesInvestimento() {
		return scanner.nextInt();
	}
	
	public void encerrarLeituraInvestimento() {
		scanner.close();
	}
}
