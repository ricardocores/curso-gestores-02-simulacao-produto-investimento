/**
 *
 * Nome do programa:			AppSimuladorInvestimento.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	16/05/2020
 * Descrição:					App de Simulação de Produto de investimento com taxa de rendimento de 0,7% / mês
 * 
 */


package com.cores.SimulaProdInvestimentos;


public class AppSimuladorInvestimento {

	public static void main(String[] args) {
		
		//Criação de objetos das classes
		InvestimentoCliente investimento
										= new InvestimentoCliente();
		LeitorInvestimento leitor		= new LeitorTerminal();  //definindo que a leitura será por terminal... poderia definir parametro de entrada para decidir
		SimuladorValorInvestido simulador	
										= new SimuladorValorInvestido();
		Impressora impressora			= new Impressora();
	
		//Imprime cabeçalho
		impressora.imprimirCabecalho();
		
		//Solicita valor a ser investido
		impressora.imprimirSolicitacaoValorInvestimento();
		investimento.setValorInvestimento(leitor.lerValorInvestimento());
		
		//Solicita quantidade de meses que o valor ficará investido
		impressora.imprimirSolicitacaoQuantidadeMesesInvestimento();
		investimento.setQtdeMesesInvestimento(leitor.lerQuantidadeMesesInvestimento());

		//Cálculo do Valor investido e Apresentação para o cliente
		impressora.imprimirValorAcumulado(simulador.simularInvestimento(investimento));
		
		//Encerra leitura, independentemente da origem
		leitor.encerrarLeituraInvestimento();
		
	}

}
