/**
 *
 * Nome do programa:			Impressora.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	17/05/2020
 * Descrição:					Imprime mensagens no terminal do cliente
 */

package com.cores.SimulaProdInvestimentos;

public class Impressora {

	public void imprimirCabecalho() {
				
		System.out.println("Simulacao de Investimento");
		System.out.println("-------------------------");
		
	}
	
	public void imprimirSolicitacaoValorInvestimento() {
		System.out.print("Digite o valor a ser investido: ");
	}
	
	public void imprimirSolicitacaoQuantidadeMesesInvestimento() {
		System.out.print("Qtde de meses em que o dinheiro ficara investido: ");
	}
	
	public void imprimirValorAcumulado(double valorAcumulado) {
		System.out.println("\nValor Acumulado: " + valorAcumulado);
	}
}
