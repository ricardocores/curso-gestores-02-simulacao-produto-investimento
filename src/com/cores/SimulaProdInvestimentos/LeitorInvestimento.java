/**
 *
 * Nome do programa:			LeitorInvestimento.java
 * Autor:                       Ricardo Cores
 * Início de desenvolvimento:	17/05/2020
 * Descrição:					Interface para generalizar a forma de leitura dos dados (Terminal, arquivo, etc)
 */

package com.cores.SimulaProdInvestimentos;

interface LeitorInvestimento {
	
	public double lerValorInvestimento();
	
	public int lerQuantidadeMesesInvestimento();
	
	public void encerrarLeituraInvestimento();
	
}
